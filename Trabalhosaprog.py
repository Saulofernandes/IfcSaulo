import pygame
import sys

# Inicializar o Pygame
pygame.init()

# Definir as cores
WHITE = (255, 255, 255)
LINE_COLOR = (0, 0, 0)
X_COLOR = (242, 85, 96)
O_COLOR = (28, 170, 156)

# Definir o tamanho da tela
SCREEN_SIZE = 600
GRID_SIZE = 3
CELL_SIZE = SCREEN_SIZE // GRID_SIZE

# Definir a fonte para desenhar X e O
FONT = pygame.font.Font(None, 150)

# Definir o estado do jogo
X_PLAYER = 'X'
O_PLAYER = 'O'
EMPTY = None

# Tela principal
screen = pygame.display.set_mode((SCREEN_SIZE, SCREEN_SIZE))
pygame.display.set_caption("Jogo da Velha")

# Função para desenhar o tabuleiro
def draw_board(board):
    # Desenhar as linhas da grade
    for row in range(1, GRID_SIZE):
        pygame.draw.line(screen, LINE_COLOR, (0, CELL_SIZE * row), (SCREEN_SIZE, CELL_SIZE * row), 5)
        pygame.draw.line(screen, LINE_COLOR, (CELL_SIZE * row, 0), (CELL_SIZE * row, SCREEN_SIZE), 5)
    
    # Desenhar X e O
    for row in range(GRID_SIZE):
        for col in range(GRID_SIZE):
            if board[row][col] == X_PLAYER:
                pygame.draw.line(screen, X_COLOR, (col * CELL_SIZE, row * CELL_SIZE), ((col + 1) * CELL_SIZE, (row + 1) * CELL_SIZE), 5)
                pygame.draw.line(screen, X_COLOR, ((col + 1) * CELL_SIZE, row * CELL_SIZE), (col * CELL_SIZE, (row + 1) * CELL_SIZE), 5)
            elif board[row][col] == O_PLAYER:
                pygame.draw.circle(screen, O_COLOR, (col * CELL_SIZE + CELL_SIZE // 2, row * CELL_SIZE + CELL_SIZE // 2), CELL_SIZE // 2, 5)

# Função para verificar se alguém venceu
def check_winner(board):
    # Verificar linhas e colunas
    for i in range(GRID_SIZE):
        if board[i][0] == board[i][1] == board[i][2] and board[i][0] is not None:
            return board[i][0]
        if board[0][i] == board[1][i] == board[2][i] and board[0][i] is not None:
            return board[0][i]

    # Verificar diagonais
    if board[0][0] == board[1][1] == board[2][2] and board[0][0] is not None:
        return board[0][0]
    if board[0][2] == board[1][1] == board[2][0] and board[0][2] is not None:
        return board[0][2]

    return None

# Função para verificar se o jogo terminou (sem vencedores)
def check_draw(board):
    for row in range(GRID_SIZE):
        for col in range(GRID_SIZE):
            if board[row][col] is None:
                return False
    return True

# Função principal do jogo
def game():
    board = [[None for _ in range(GRID_SIZE)] for _ in range(GRID_SIZE)]
    current_player = X_PLAYER
    game_over = False

    while True:
        screen.fill(WHITE)
        draw_board(board)

        if game_over:
            # Exibir o resultado do jogo
            if winner := check_winner(board):
                result_text = FONT.render(f"{winner} venceu!", True, LINE_COLOR)
            else:
                result_text = FONT.render("Empate!", True, LINE_COLOR)
            screen.blit(result_text, (SCREEN_SIZE // 4, SCREEN_SIZE // 2 - 100))
            pygame.display.update()
            pygame.time.delay(2000)
            game()
        
        # Verificar os eventos do jogo
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                if not game_over:
                    mouse_x, mouse_y = event.pos
                    clicked_row = mouse_y // CELL_SIZE
                    clicked_col = mouse_x // CELL_SIZE

                    if board[clicked_row][clicked_col] is None:
                        board[clicked_row][clicked_col] = current_player
                        
                        # Verificar se há um vencedor
                        if check_winner(board):
                            game_over = True
                        elif check_draw(board):
                            game_over = True

                        # Trocar o jogador
                        current_player = O_PLAYER if current_player == X_PLAYER else X_PLAYER

        # Atualizar a tela
        pygame.display.update()

# Iniciar o jogo
game()

# Encerrar o Pygame
pygame.quit()
